#!/bin/sh

#
# Inputs
#

enconter="./vampyr-utils/pascal/ENCONTER.exe"
sign="./vampyr-utils/pascal/sign.exe"
mondat="./vampyr-utils/pascal/mondat.exe"

version_dir="v2.0"
out_dir="csv_$version_dir"

#
# Program
#

set -e

if [ ! -f "$enconter" -o ! -f "$sign" -o ! -f "$mondat" ]
then
	echo "Could not find required programs."
	echo " * Are you running from repo root?"
	echo " * Did you compile the Pascal programs?"
	exit 1
fi

input_dir="$version_dir/game"

mkdir -p "$out_dir"

echo "ENCONTER.SET"
"$enconter" "$input_dir/ENCONTER.SET" > "$out_dir/ENCONTER.SET.csv"
echo "SIGN.DAT"
"$sign" "$input_dir/SIGN.DAT" > "$out_dir/SIGN.DAT.csv"

for file in DUNGMON.DAT LANDMON.DAT LIFEMON.DAT RUINMON.DAT TOWNMON.DAT
do
	echo "$file"
	"$mondat" "$input_dir/${file}" > "$out_dir/${file}.csv"
done
