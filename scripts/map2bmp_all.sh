#!/bin/sh

#
# Input
#

version_dir="v2.0"

gfx_dir="gfx_$version_dir"
csv_dir="csv_$version_dir"
out_dir="maps_$version_dir"

stitchss="./build/vampyr-utils/cppsrc/stitchss/stitchss"
map2bmp="./build/vampyr-utils/cppsrc/map2bmp/map2bmp"
map_layer_enconter="./build/vampyr-utils/cppsrc/map_layer_enconter/map_layer_enconter"

#
# Program
#

set -e

game_dir="$version_dir/game"

if [ ! -d "$gfx_dir" -o ! -d "$csv_dir" ]
then
    # TODO: Pre-check
    echo "Expecting more dirs"
    exit 1
fi

mkdir -p "$out_dir"

univ7="$out_dir/tmp0.bmp"
univ8="$out_dir/tmp1.bmp"
tilesheet="$out_dir/tmp2.bmp"

# The overworld only wants the first 7 images of UNIV.CON
"$stitchss" "$univ7" \
    "$gfx_dir/UNIV/0.bmp" \
    "$gfx_dir/UNIV/1.bmp" \
    "$gfx_dir/UNIV/2.bmp" \
    "$gfx_dir/UNIV/3.bmp" \
    "$gfx_dir/UNIV/4.bmp" \
    "$gfx_dir/UNIV/5.bmp" \
    "$gfx_dir/UNIV/6.bmp"
# The rest want the 8 images of UNIV.CON
"$stitchss" "$univ8" "$univ7" "$gfx_dir/UNIV/7.bmp"

echo "WORLD.MAP"
"$stitchss" "$tilesheet" "$univ7" "$gfx_dir/LAND.CON.bmp"
"$map2bmp" "$game_dir/WORLD.MAP" 110 100 "$tilesheet" "$out_dir/WORLD.MAP.bmp"
# WORLD.MAP has no NPCs

echo "AFTER.MAP"
"$stitchss" "$tilesheet" "$univ8" "$gfx_dir/AFTER.CON.bmp"
"$map2bmp" "$game_dir/AFTER.MAP" 50 50 "$tilesheet" "$out_dir/AFTER.MAP.bmp"
"$map_layer_enconter" "$out_dir/AFTER.MAP.bmp" "$csv_dir/ENCONTER.SET.csv" 226 250 "$gfx_dir/LIFEMON.CON.bmp" "$out_dir/AFTER.MAP.bmp"

echo "THEEND.MAP"
"$stitchss" "$tilesheet" "$univ8" "$gfx_dir/THEEND.CON.bmp"
"$map2bmp" "$game_dir/THEEND.MAP" 50 50 "$tilesheet" "$out_dir/THEEND.MAP.bmp"
# THEEND.MAP has no NPCs

# $stitchss "${tilesheet}" "${univ}" images/dungeon_show_secrets.bmp
# $map2bmp RUIN.MAP 100 50 "${tilesheet}" maps/RUIN.MAP.bmp
# $map2bmp VCASTLE.MAP 150 50 "${tilesheet}" maps/VCASTLE.MAP.bmp
# $map2bmp DUNGEON.MAP 300 50 "${tilesheet}" maps/DUNGEON.MAP.bmp

# $stitchss "${tilesheet}" "${univ}" images/town_show_secrets.bmp
# $map2bmp CASTLE.MAP 150 50 "${tilesheet}" maps/CASTLE.MAP.bmp
# $map2bmp TOWN.MAP 300 50 "${tilesheet}" maps/TOWN.MAP.bmp
