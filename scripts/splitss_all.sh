#!/bin/bash

#
# Inputs
#

version_dir="v2.0"

input_dir="gfx_$version_dir"
out_dir="gfx_$version_dir"

splitss="./build/vampyr-utils/cppsrc/splitss/splitss.exe"

#
# Program
#

set -e

if [ ! -f "$splitss" ]
then
	echo "Could not find splitss, tried: $splitss"
	echo "Are you running from repo root?"
	exit 1
fi

if [ ! -d "$input_dir" ]
then
	echo "Could not find input dir, tried: $input_dir"
	echo "Have you run con2bmp_all.sh first?"
	exit 1
fi

mkdir -p "$out_dir"

for ss in AFTER DUNGEON DUNGMON ENDMON LAND LANDMON PLAYER RUINMON THEEND TOWN TOWNMON UNIV
do
	echo "$ss"
	"$splitss" "$input_dir/$ss.CON.bmp" 18 18 "$out_dir/$ss"
done
