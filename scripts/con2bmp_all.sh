#!/bin/bash

#
# Inputs
#

version_dir="v2.0"

out_dir="gfx_$version_dir"

con2bmp="./build/vampyr-utils/cppsrc/con2bmp/con2bmp.exe"

#
# Program
#

set -e

if [ ! -f "$con2bmp" ]
then
	echo "Could not find con2bmp, tried: $con2bmp"
	echo "Are you running from repo root?"
	exit 1
fi

input_dir="$version_dir/game"

mkdir -p "$out_dir"

# The resolution of the file is hard-coded into the EXE
for file in AFTER.CON DUNGEON.CON LAND.CON THEEND.CON TOWN.CON
do
	echo "$file"
	"$con2bmp" "$input_dir/$file" 360 18 "$out_dir/$file.bmp"
done

for file in DUNGMON.CON ENDMON.CON LANDMON.CON LIFEMON.CON RUINMON.CON TOWNMON.CON
do
	echo "$file"
	"$con2bmp" "$input_dir/$file" 468 18 "$out_dir/$file.bmp"
done

for file in PLAYER.CON UNIV.CON
do
	echo "$file"
	"$con2bmp" "$input_dir/$file" 270 18 "$out_dir/$file.bmp"
done

for file in TITLE.001 TITLE.002 TITLE.003 TITLE.004 TITLE.005 TITLE.006
do
	echo "$file"
	"$con2bmp" "$input_dir/$file" 106 99 "$out_dir/$file.bmp"
done

for file in ITEM.001 ITEM.002 ITEM.003 ITEM.004 ITEM.005 ITEM.006 ITEM.007
do
	echo "$file"
	"$con2bmp" "$input_dir/$file" 25 25 "$out_dir/$file.bmp"
done

echo "VAMPYR.001"
"$con2bmp" "$input_dir/VAMPYR.001" 145 25 "$out_dir/VAMPYR.001.bmp"
echo "MSCROLL.PIC"
"$con2bmp" "$input_dir/MSCROLL.PIC" 25 77 "$out_dir/MSCROLL.PIC.bmp"

# The text should actually be black on this one but eh (shrugs)
echo "SCROLL.PIC"
"$con2bmp" "$input_dir/SCROLL.PIC" 290 77 "$out_dir/SCROLL.PIC.bmp"
