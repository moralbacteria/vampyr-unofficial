# Vampyr Unofficial

Patches and fixes for Vampyr: Talisman of Invocation.

For more information, see the
[unofficial wiki](https://vampyr-talisman-of-invocation.fandom.com/)
hosted on Fandom. I'd recommend the
[Reverse Engineering](https://vampyr-talisman-of-invocation.fandom.com/wiki/Reverse_Engineering)
section.

## !!! IMPORTANT: SUBMODULES !!!

This repo uses submodules so please clone recursively.

## Requirements

* C++17 compiler
* CMake 3.10+
* Free Pascal

## Getting Started

This repo relies on tools in the `vampyr-utils` submodule. They must be compiled first.

First, compile the C++ tools:

    mkdir build
    cd build
    cmake ..
    make

Second, compile the Pascal tools:

    make -C vampyr-utils/pascal

## Scripts

The `scripts/` folder has a bunch of useful scripts for decoding data en mass.

These scripts must be run from the repo root!

  * `con2bmp_all.sh`: Convert all image files (`.CON` and others) to BMP
  * `splitss_all.sh`: Takes the output of `con2bmp_all.sh` and breaks down the sprite sheets into individual images

## Fixes

Notable fixes include:

 * DUNGEON.CON uses the v2.0 door tile
 * THEEND.CON uses the v2.0 marsh, vcastle tiles
