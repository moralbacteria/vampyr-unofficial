#!/bin/sh

set -e

if [ ! -e VAMPYR.EXE ]
then
	echo Please run from repo root
	exit 1
fi

stitchss=./vampyr-utils/cppsrc/stitchss/stitchss
map2bmp=./vampyr-utils/cppsrc/map2bmp/map2bmp

mkdir -p maps

univ=$(mktemp)

#
# The overworld only wants the first 7 images of UNIV.CON
#

$stitchss "${univ}" \
	tiles/water-deep0.bmp \
	tiles/water0.bmp \
	tiles/bridge.bmp \
	tiles/trees.bmp \
	tiles/shrubs.bmp \
	tiles/sign.bmp \
	tiles/grass.bmp

tilesheet=$(mktemp)

$stitchss "${tilesheet}" "${univ}" images/LAND.CON.bmp
$map2bmp WORLD.MAP 110 100 "${tilesheet}" maps/WORLD.MAP.bmp

#
# The rest want the first eight
#

$stitchss "${univ}" "${univ}" tiles/chest.bmp

$stitchss "${tilesheet}" "${univ}" images/AFTER.CON.bmp
$map2bmp AFTER.MAP 50 50 "${tilesheet}" maps/AFTER.MAP.bmp

$stitchss "${tilesheet}" "${univ}" images/THEEND.CON.bmp
$map2bmp THEEND.MAP 50 50 "${tilesheet}" maps/THEEND.MAP.bmp

$stitchss "${tilesheet}" "${univ}" images/dungeon_show_secrets.bmp
$map2bmp RUIN.MAP 100 50 "${tilesheet}" maps/RUIN.MAP.bmp
$map2bmp VCASTLE.MAP 150 50 "${tilesheet}" maps/VCASTLE.MAP.bmp
$map2bmp DUNGEON.MAP 300 50 "${tilesheet}" maps/DUNGEON.MAP.bmp

$stitchss "${tilesheet}" "${univ}" images/town_show_secrets.bmp
$map2bmp CASTLE.MAP 150 50 "${tilesheet}" maps/CASTLE.MAP.bmp
$map2bmp TOWN.MAP 300 50 "${tilesheet}" maps/TOWN.MAP.bmp
