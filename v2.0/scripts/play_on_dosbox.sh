#!/bin/bash

dosbox=$HOME/svn/dosbox-debug/src/dosbox

if [ ! -e VAMPYR.EXE ]
then
	echo Please run from repo root
	exit 1
fi

$dosbox -conf scripts/dosbox.conf
