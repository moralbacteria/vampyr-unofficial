#!/bin/bash

set -e

if [ ! -e VAMPYR.EXE ]
then
	echo "Please run from repo root"
	exit 1
fi

stitchss=./vampyr-utils/cppsrc/stitchss/stitchss

$stitchss images/AFTER.CON.bmp \
	tiles/brazier0.bmp \
	tiles/stone-path.bmp \
	tiles/wall-brick.bmp \
	tiles/shop-arm.bmp \
	tiles/tile-floor.bmp \
	tiles/fence.bmp \
	tiles/computer.bmp \
	tiles/tile-pillar.bmp \
	tiles/door.bmp \
	tiles/door-locked.bmp \
	tiles/sky-clouds.bmp \
	tiles/window.bmp \
	tiles/tile-ash.bmp \
	tiles/cobblestone.bmp \
	tiles/store-counter.bmp \
	tiles/sky.bmp \
	tiles/shop-wpn.bmp \
	tiles/justice0.bmp \
	tiles/justice1.bmp \
	tiles/brazier1.bmp

$stitchss images/DUNGEON.CON.bmp \
	tiles/brazier0.bmp \
	tiles/lava0.bmp \
	tiles/wall-ornament.bmp \
	tiles/tomb.bmp \
	tiles/grave.bmp \
	tiles/stairs-up.bmp \
	tiles/stairs-down.bmp \
	tiles/marsh.bmp \
	tiles/stone-path.bmp \
	tiles/wall-brick.bmp \
	tiles/wall-brick.bmp \
	tiles/tile-floor.bmp \
	tiles/tile-pillar.bmp \
	tiles/door.bmp \
	tiles/window-broken.bmp \
	tiles/cobblestone.bmp \
	tiles/mountain-face.bmp \
	tiles/wall-smooth.bmp \
	tiles/brazier1.bmp \
	tiles/lava1.bmp

$stitchss images/dungeon_show_secrets.bmp \
	tiles/brazier0.bmp \
	tiles/lava0.bmp \
	tiles/wall-ornament.bmp \
	tiles/tomb.bmp \
	tiles/grave.bmp \
	tiles/stairs-up.bmp \
	tiles/stairs-down.bmp \
	tiles/marsh.bmp \
	tiles/stone-path.bmp \
	tiles/wall-brick.bmp \
	tiles/__secret_door.bmp \
	tiles/tile-floor.bmp \
	tiles/tile-pillar.bmp \
	tiles/door.bmp \
	tiles/window-broken.bmp \
	tiles/cobblestone.bmp \
	tiles/mountain-face.bmp \
	tiles/wall-smooth.bmp \
	tiles/brazier1.bmp \
	tiles/lava1.bmp

$stitchss images/LAND.CON.bmp \
	tiles/vcastle-top0.bmp \
	tiles/town-left0.bmp \
	tiles/town-right0.bmp \
	tiles/castle-left0.bmp \
	tiles/ruins-left0.bmp \
	tiles/mountain.bmp \
	tiles/mountain-entrance.bmp \
	tiles/marsh.bmp \
	tiles/palms.bmp \
	tiles/boat.bmp \
	tiles/castle-right.bmp \
	tiles/ruins-right.bmp \
	tiles/vcastle-bottom0.bmp \
	tiles/hills.bmp \
	tiles/plains.bmp \
	tiles/vcastle-top1.bmp \
	tiles/town-left1.bmp \
	tiles/town-right1.bmp \
	tiles/castle-left1.bmp \
	tiles/ruins-left1.bmp

$stitchss images/THEEND.CON.bmp \
	tiles/wall-brick.bmp \
	tiles/stone-path.bmp \
	tiles/tile-floor.bmp \
	tiles/store-counter.bmp \
	tiles/window.bmp \
	tiles/tile-pillar.bmp \
	tiles/vcastle-top0.bmp \
	tiles/vcastle-bottom0.bmp \
	tiles/sand.bmp \
	tiles/sand-umbrella.bmp \
	tiles/marsh.bmp \
	tiles/mountain.bmp \
	tiles/vcastle-top2.bmp \
	tiles/vcastle-bottom1.bmp \
	tiles/vcastle-top3.bmp \
	tiles/vcastle-bottom2.bmp \
	tiles/vcastle-top4.bmp \
	tiles/vcastle-bottom3.bmp \
	tiles/vcastle-top5.bmp \
	tiles/vcastle-bottom4.bmp \

$stitchss images/TOWN.CON.bmp \
	tiles/wall-brick.bmp \
	tiles/stone-path.bmp \
	tiles/wall-brick.bmp \
	tiles/tile-floor.bmp \
	tiles/window.bmp \
	tiles/wall-smooth.bmp \
	tiles/door.bmp \
	tiles/shop-pub.bmp \
	tiles/shop-arm.bmp \
	tiles/shop-wpn.bmp \
	tiles/shop-tnp.bmp \
	tiles/shop-trn.bmp \
	tiles/shop-inn.bmp \
	tiles/store-counter.bmp \
	tiles/tile-pillar.bmp \
	tiles/stairs-down.bmp \
	tiles/stairs-up.bmp \
	tiles/door-locked.bmp \
	tiles/boat.bmp \
	tiles/fence.bmp

$stitchss images/town_show_secrets.bmp \
	tiles/__secret_door.bmp \
	tiles/stone-path.bmp \
	tiles/wall-brick.bmp \
	tiles/tile-floor.bmp \
	tiles/window.bmp \
	tiles/wall-smooth.bmp \
	tiles/door.bmp \
	tiles/shop-pub.bmp \
	tiles/shop-arm.bmp \
	tiles/shop-wpn.bmp \
	tiles/shop-tnp.bmp \
	tiles/shop-trn.bmp \
	tiles/shop-inn.bmp \
	tiles/store-counter.bmp \
	tiles/tile-pillar.bmp \
	tiles/stairs-down.bmp \
	tiles/stairs-up.bmp \
	tiles/door-locked.bmp \
	tiles/boat.bmp \
	tiles/fence.bmp

$stitchss images/UNIV.CON.bmp \
	tiles/water-deep0.bmp \
	tiles/water0.bmp \
	tiles/bridge.bmp \
	tiles/trees.bmp \
	tiles/shrubs.bmp \
	tiles/sign.bmp \
	tiles/grass.bmp \
	tiles/chest.bmp \
	tiles/fx-fireball.bmp \
	tiles/fx-arrow.bmp \
	tiles/fx-lightning.bmp \
	tiles/fx-rock.bmp \
	tiles/water-deep1.bmp \
	tiles/water1.bmp \
	tiles/fx-magic-missile.bmp
