#!/bin/bash

set -e

if [ ! -e VAMPYR.EXE ]
then
	echo Please run from repo root
	exit 1
fi

map_layer_enconter=./vampyr-utils/cppsrc/map_layer_enconter/map_layer_enconter
splitss=./vampyr-utils/cppsrc/splitss/splitss

tmpdir="${TMPDIR:-/tmp}/vampyrutils"

mkdir -p maps_with_npcs

#
# All offsets came from poking around VAPMYR.EXE disassembly
#

"${map_layer_enconter}" maps/AFTER.MAP.bmp csv/ENCONTER.SET.csv 226 250 images/LIFEMON.CON.bmp maps_with_npcs/after.bmp

"${splitss}" maps/RUIN.MAP.bmp 900 900 "${tmpdir}"
"${map_layer_enconter}" "${tmpdir}/0.bmp" csv/ENCONTER.SET.csv 256 265 images/RUINMON.CON.bmp maps_with_npcs/ruins-mountains.bmp
"${map_layer_enconter}" "${tmpdir}/1.bmp" csv/ENCONTER.SET.csv 251 255 images/RUINMON.CON.bmp maps_with_npcs/ruins-forest.bmp

"${splitss}" maps/CASTLE.MAP.bmp 900 900 "${tmpdir}"
"${map_layer_enconter}" "${tmpdir}/0.bmp" csv/ENCONTER.SET.csv 151 175 images/TOWNMON.CON.bmp maps_with_npcs/castle-2.bmp
"${map_layer_enconter}" "${tmpdir}/1.bmp" csv/ENCONTER.SET.csv 176 200 images/TOWNMON.CON.bmp maps_with_npcs/castle-1.bmp
"${map_layer_enconter}" "${tmpdir}/2.bmp" csv/ENCONTER.SET.csv 201 225 images/TOWNMON.CON.bmp maps_with_npcs/castle-g.bmp

"${splitss}" maps/VCASTLE.MAP.bmp 900 900 "${tmpdir}"
"${map_layer_enconter}" "${tmpdir}/0.bmp" csv/ENCONTER.SET.csv 272 274 images/RUINMON.CON.bmp maps_with_npcs/vcastle-1.bmp
"${map_layer_enconter}" "${tmpdir}/1.bmp" csv/ENCONTER.SET.csv 275 277 images/RUINMON.CON.bmp maps_with_npcs/vcastle-g.bmp
"${map_layer_enconter}" "${tmpdir}/2.bmp" csv/ENCONTER.SET.csv 278 280 images/RUINMON.CON.bmp maps_with_npcs/vcastle-b.bmp

"${splitss}" maps/DUNGEON.MAP.bmp 900 900 "${tmpdir}"
"${map_layer_enconter}" "${tmpdir}/0.bmp" csv/ENCONTER.SET.csv 266 267 images/DUNGMON.CON.bmp maps_with_npcs/dungeon-talisman-g.bmp
"${map_layer_enconter}" "${tmpdir}/1.bmp" csv/ENCONTER.SET.csv 268 269 images/DUNGMON.CON.bmp maps_with_npcs/dungeon-talisman-b1.bmp
"${map_layer_enconter}" "${tmpdir}/2.bmp" csv/ENCONTER.SET.csv 270 271 images/DUNGMON.CON.bmp maps_with_npcs/dungeon-talisman-b2.bmp

"${splitss}" maps/TOWN.MAP.bmp 900 900 "${tmpdir}"
"${map_layer_enconter}" "${tmpdir}/0.bmp" csv/ENCONTER.SET.csv 1 25 images/TOWNMON.CON.bmp maps_with_npcs/town-balinar.bmp
"${map_layer_enconter}" "${tmpdir}/1.bmp" csv/ENCONTER.SET.csv 26 50 images/TOWNMON.CON.bmp maps_with_npcs/town-rendyr.bmp
"${map_layer_enconter}" "${tmpdir}/2.bmp" csv/ENCONTER.SET.csv 51 75 images/TOWNMON.CON.bmp maps_with_npcs/town-maninox.bmp
"${map_layer_enconter}" "${tmpdir}/3.bmp" csv/ENCONTER.SET.csv 76 100 images/TOWNMON.CON.bmp maps_with_npcs/town-zachul.bmp
"${map_layer_enconter}" "${tmpdir}/4.bmp" csv/ENCONTER.SET.csv 101 125 images/TOWNMON.CON.bmp maps_with_npcs/town-trocines.bmp
"${map_layer_enconter}" "${tmpdir}/5.bmp" csv/ENCONTER.SET.csv 126 150 images/TOWNMON.CON.bmp maps_with_npcs/town-myron.bmp
"${map_layer_enconter}" "${tmpdir}/5.bmp" csv/ENCONTER.SET.csv 281 298 images/TOWNMON.CON.bmp maps_with_npcs/town-myron-invaded.bmp
