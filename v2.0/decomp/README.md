## Building

The build system is early

Requires:

* Borland Turbo Pascal 5.0 installed to I:\TP
* DOSBox extracted to I:\DOSBox
* Source in C:\Users\Michael\Desktop\git\vampyr-unofficial\v2.0\decomp

Alternatively, you can set these in `tpenv.cfg`

Steps:

1. Run tpenv.bat
2. In DOSBox, run `make`
