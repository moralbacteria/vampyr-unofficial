## v2.0 Tiles

This folder contains all v2.0 tiles as individual .BMP files. It does not include
the v1.1 tiles that accidentally slipped into the v2.0 release.

The contents were manually created by doing the following:

1. Run `scripts/con2bmp_all.sh` to convert all .CON to .BMP files.
2. Run `scripts/splitss_all.sh` to convert the large tilesheets into individual images.
3. Rename each tile image
4. Deduplicate
5. Throw away v1.1 images

> `__secret_door.bmp` is `door.bmp` with the colors inverted so it stands out in maps

These tiles are used by other scripts:

  * `stitch_sprite_sheets.sh`
  * `convert_maps.sh`
