# IDA 7.0 Freeware Disassembly

IDA 7.0 Freeware dropped support for autoanalysis of 16 bit Pascal code. So I had to do a bunch of stuff manually:

1. Remove everything up to the first code segment (MZ header, reloc, etx). **Code starts at 0x3F10**
2. Open in IDA and do disassembly
3. Manually specify segments
4. Manually denote function boundaries
5. Manually define Pascal strings
6. Fix XREF offsets

It's not entirely perfect, I don't think I've done all the Borland code.
