# Change main menu version

> NOTE TO SELF: Beware the relocation table! Since we're removing function calls, there are a lot of dangling entries in there!!!

## Code Changes

The existing main menu code basically does this:

```
OutputTextXY(267, 125, 'V'+'e'+'r'+'s'+'i'+'o'+'n'+' '+'2'+'.'+'0')
```

Which expands to _a lot_ of instructions. Replace that with our own version string and equivalent drawing function:

(Since we overwrite data used by the copyright printer, it was removed)

```
; String data for function starts at binary offset 0x22ed1
vampyrMainMenu:2B61 0E                            version         db 14,'UNOFFICIAL 0.1'
...

; Code starts at binary offset 0x23183
vampyrMainMenu:2E13 B8 0B 01                                      mov     ax, 267 ; y
vampyrMainMenu:2E16 50                                            push    ax
vampyrMainMenu:2E17 B8 7D 00                                      mov     ax, 125 ; x
vampyrMainMenu:2E1A 50                                            push    ax
vampyrMainMenu:2E1B BF 61 2B                                      mov     di, offset aUnofficial01 ; "UNOFFICIAL 0.1"
vampyrMainMenu:2E1E 0E                                            push    cs
vampyrMainMenu:2E1F 57                                            push    di
vampyrMainMenu:2E20 9A 06 18 98 21                                call    OutputTextXY ; REQUIRES RELOC TABLE ENTRY
vampyrMainMenu:2E25 EB 70                                         jmp     short options
...

; BINARY address 0x23207
vampyrMainMenu:2E97 B8 EB 00                      options:        mov     ax, 0EBh ; offset 0x23207 in a hex editor
                                                                                   ; this is the same up until the jmp
vampyrMainMenu:2E9A 50                                            push    ax
vampyrMainMenu:2E9B B8 8C 00                                      mov     ax, 8Ch
vampyrMainMenu:2E9E 50                                            push    ax
vampyrMainMenu:2E9F BF 75 2B                                      mov     di, offset aJOurneyOnward ; "[J]ourney Onward"
vampyrMainMenu:2EA2 0E                                            push    cs
vampyrMainMenu:2EA3 57                                            push    di
vampyrMainMenu:2EA4 9A 06 18 98 21                                call    OutputTextXY
vampyrMainMenu:2EA9 B8 EB 00                                      mov     ax, 0EBh
vampyrMainMenu:2EAC 50                                            push    ax
vampyrMainMenu:2EAD B8 99 00                                      mov     ax, 99h
vampyrMainMenu:2EB0 50                                            push    ax
vampyrMainMenu:2EB1 BF 86 2B                                      mov     di, offset aCReateANewPlay ; "[C]reate a New Player"
vampyrMainMenu:2EB4 0E                                            push    cs
vampyrMainMenu:2EB5 57                                            push    di
vampyrMainMenu:2EB6 9A 06 18 98 21                                call    OutputTextXY
vampyrMainMenu:2EBB B8 EB 00                                      mov     ax, 0EBh
vampyrMainMenu:2EBE 50                                            push    ax
vampyrMainMenu:2EBF B8 A6 00                                      mov     ax, 0A6h
vampyrMainMenu:2EC2 50                                            push    ax
vampyrMainMenu:2EC3 BF 9C 2B                                      mov     di, offset aACknowledgemen ; "[A]cknowledgements"
vampyrMainMenu:2EC6 0E                                            push    cs
vampyrMainMenu:2EC7 57                                            push    di
vampyrMainMenu:2EC8 9A 06 18 98 21                                call    OutputTextXY
vampyrMainMenu:2ECD E9 1B 02                                      jmp     pause ; !!! This is different! 0x2323D
                                                                                ; We jump becase we overrode some data used by the copyright printer
...

; binary address 0x2345b
vampyrMainMenu:30EB 9A 0C 03 DA 25                pause:          call    ReadKey ; no change, just included for offset
```

## Relocation Table Changes

Since we're removing function calls, there are a lot of dangling entries in the relocation table.

We can use one of them to relocate the `call OutputTextXY` that we added.

Change binary offset 0x3774 to `23 2E 46 1C`
