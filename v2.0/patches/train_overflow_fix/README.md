# Training Skill Points Overflow Fix

While training, you may not get the right amount of stat points.

Normally, you get 45 each level. If you need less than that to max out all the stats for that trainer then the game will only give you want you need. For example, if the trainer only does fighting attack and your fighting attack is 80, then you will only get 20 points.

To do this, the game looks at each stat that the trainer offers and accumulates in a single byte (8 bits) how many stat points the player would need to max out all those stats. A single byte (which ranges from 0 to 255) is not enough space to hold this sum!

Consider this: if all your stats are at 1, and the trainer trains three stats, then in order to max out those stats you would need (100-1) + (100-1) + (100-1) = 99+99+99 = 297 stat points. This is greater than 255 and will overflow to 42! So even though you should get 45 stat points, the game will give you 42. What's worse is that next level you will get (100-43)+(100-1)+(100-1) mod 255 = 57+99+99 mod 255 = 255 mod 255 = 0, so you get nothing!

This can be worked around by going to a different trainer.

## Fix: Use 16 bits for accumulation

We can make room on the stack then switch calculations to use that.

```
; Starting at binary offset 0xC820
seg836:0542 83 EC 22                                      sub     sp, 22h
...
seg836:05DC                                               mov     [bp+NewVariable], 0       ; This the variable that we made room for
                                                                                            ; -21 is DF
...
seg836:060C                                               mov     ax, [bp+NewVariable]      ; statAccum += (100 - stat)
seg836:0611                                               add     ax, dx
seg836:0613                                               mov     [bp+NewVariable], ax
...
seg836:061C                                               cmp     [bp+NewVariable], 45
...
seg836:0629                                               mov     ax, [bp+NewVariable]      ; If (StatAccum < 45) StatPoints = StatAccum
...

```