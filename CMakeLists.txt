cmake_minimum_required(VERSION 3.10)
project(vampyr_unofficial)

add_subdirectory(open-vcdiff)
add_subdirectory(tputil)
add_subdirectory(vampyr-utils)
