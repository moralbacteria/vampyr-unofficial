# TPU Utilites for Borland Turbo Pascal 5.0

Confirmed working with:

* GRAPH.TPU
* CRT.TPU (from TURBO.TPL)

## Building

Requires a C++14 compiler.

```
mkdir build
cd build
cmake ..
make
```

## Utilites

* `tpupeek`: Find the TPU file offset that corresponds to the x86 code for that symbol (only works with functions)
* `tpudump`: Print all function symbols and the TPU file offset of the x86 code (sorted by file offset)
