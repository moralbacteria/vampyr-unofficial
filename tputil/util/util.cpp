#include "util.hpp"

#include <iomanip>
#include <iostream>
#include <iterator>
#include <fstream>
#include <stdexcept>
#include <vector>

std::vector<uint8_t> read_binary_file(std::string const& path)
{
  std::ifstream file(path, std::ios_base::binary);
  if (!file.good()) {
      throw std::runtime_error("Failed to open: " + path);
  }
  return std::vector<uint8_t>(std::istreambuf_iterator<char>(file), std::istreambuf_iterator<char>());
}

std::vector<uint8_t> to_pascal_string(std::string const& cppstr)
{
    if (cppstr.size() > 255) {
        throw std::invalid_argument("cppstr must be <= 255 chars");
    }

    std::vector<uint8_t> builder;
    builder.reserve(cppstr.size() + 1);
    builder.push_back(static_cast<uint8_t>(cppstr.size()));
    for (auto c : cppstr) {
        builder.push_back(c);
    }
    return builder;
}

std::string from_pascal_string(std::vector<uint8_t> const& buffer, size_t const pos)
{
    if (pos >= buffer.size()) {
        throw std::invalid_argument{"pos exceeds data length"};
    }

    auto iter = std::next(buffer.cbegin(), pos);

    auto const pstr_length = *iter;
    std::advance(iter, 1);
    // TODO: bounds check

    return std::string{iter, std::next(iter, pstr_length)};
}

uint16_t get_word(std::vector<uint8_t> const& buffer, size_t pos)
{
    if (pos + 1 >= buffer.size()) {
        throw std::invalid_argument("pos would overrun buffer");
    }
    return buffer[pos] | (buffer[pos + 1] << 8);
}
