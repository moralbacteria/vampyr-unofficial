#pragma once

#include "util.hpp"

#include <functional>
#include <stdexcept>
#include <string>
#include <vector>

class tpu_file {
public:
    tpu_file(std::vector<uint8_t> data);

    /// @return 0 on failure, absolute offset to code start otherwise
    uint16_t find_code_pointer(std::string const& symbol_name) const;

    struct NameOffsetPair {
        std::string name;
        uint16_t offset;
    };
    std::vector<NameOffsetPair> get_all_functions() const;

private:
    struct symbol_wrapper {
        // Typical structure:
        //
        // next - word
        // name - pstr (byte length + string data length)
        // type - char
        // ???  - type dependent data

        tpu_file const& file;
        uint16_t pointer;

        uint16_t get_next() const
        {
            return file.read_word(pointer);
        }

        std::string get_name() const
        {
            return from_pascal_string(file._data, pointer + sizeof(uint16_t));
        }

        uint16_t get_name_length() const
        {
            return file._data[pointer + sizeof(uint16_t)];
        }

        char get_type() const
        {
            auto const type_offset = sizeof(uint16_t) + sizeof(uint8_t) + get_name_length();
            return file._data[pointer + type_offset];
        }

        uint16_t get_proc_map_offset() const
        {
            auto const symbol_type = get_type();
            if (symbol_type != 'T' && symbol_type != 'U') {
                throw std::runtime_error{"Can't get procmap for non-T/non-U symbol"};
            }

            auto field_offset = sizeof(uint16_t) + sizeof(uint8_t) + get_name_length() + sizeof(char) + sizeof(uint8_t);
            return file.read_word(pointer + field_offset);
        }
    };

    // callback return - return true to keep walking, false to stop
    // callback param - the symbol offset, suitable to be given to symbol_wrapper
    // return - OR'd bubble up (short circuited) of all callback returns
    bool walk_hash_table(std::function<bool(uint16_t)> const& callback) const;

    bool walk_hash_list(uint16_t symbol_pointer, std::function<bool(uint16_t)> const& callback) const;

    uint16_t read_word(uint16_t const offset) const;

    uint16_t resolve_code_from_proc(uint16_t const proc_offset) const;

    std::vector<uint8_t> _data;

    uint16_t _hash_map_base = 0;
    uint16_t _cseg_map_base = 0;
    uint16_t _proc_map_base = 0;
    uint16_t _code_base_para = 0;
};
