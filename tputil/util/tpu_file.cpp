#include "tpu_file.hpp"

#include <functional>
#include <stdexcept>
#include <string>
#include <vector>

tpu_file::tpu_file(std::vector<uint8_t> data)
: _data{std::move(data)}
{
    if (_data.empty()) {
        throw std::invalid_argument{"expected non-empty data"};
    }

    // Parse header for future convenience
    _hash_map_base = read_word(0xA);
    _cseg_map_base = read_word(0xC);
    _proc_map_base = read_word(0xE);

    auto const code_base = read_word(0x18);
    // The first segment is actually paragraph aligned, although the TPU
    // header doesn't store it this way... Gee, thanks folks.
    _code_base_para = ((code_base >> 4) + 1) << 4;
}

/// @return 0 on failure, absolute offset to code start otherwise
uint16_t tpu_file::find_code_pointer(std::string const& symbol_name) const
{
    // Step 1: find symbol
    uint16_t desired_pointer = 0;
    walk_hash_table([this, &desired_pointer, &symbol_name](uint16_t const symbol_pointer) {
        symbol_wrapper const wrapper{*this, symbol_pointer};

        auto const symbol_type = wrapper.get_type();
        if (symbol_type != 'T' && symbol_type != 'U') {
            return false;
        }

        if (wrapper.get_name() != symbol_name) {
            return false;
        }

        desired_pointer = symbol_pointer;
        return true;
    });

    if (desired_pointer == 0) {
        return 0;
    }

    return resolve_code_from_proc(symbol_wrapper{*this, desired_pointer}.get_proc_map_offset());
}

std::vector<tpu_file::NameOffsetPair> tpu_file::get_all_functions() const
{
    std::vector<NameOffsetPair> builder;
    walk_hash_table([this, &builder](uint16_t const symbol_pointer) {
        symbol_wrapper const wrapper{*this, symbol_pointer};

        auto const symbol_type = wrapper.get_type();
        if (symbol_type != 'T' && symbol_type != 'U') {
            return false;
        }

        builder.push_back({wrapper.get_name(), resolve_code_from_proc(wrapper.get_proc_map_offset())});

        // Don't short circuit, keep going so we read the whole table
        return false;
    });
    return builder;
}

bool tpu_file::walk_hash_table(std::function<bool(uint16_t)> const& callback) const
{
    constexpr auto entry_size = sizeof(uint16_t);

    // Hash table size is always (size-2), which is why we add 2.
    // Total size doesn't account for the first word either, but we don't
    // care bceause we start reading past the size word.
    auto const hash_table_size = read_word(_hash_map_base) + entry_size;
    auto const first_entry = _hash_map_base + entry_size;

    for (auto i = first_entry; i < first_entry + hash_table_size; i += sizeof(uint16_t)) {
        auto symbol_pointer = read_word(i);

        if (walk_hash_list(symbol_pointer, callback)) {
            return true;
        }
    }

    return false;
}

bool tpu_file::walk_hash_list(uint16_t symbol_pointer, std::function<bool(uint16_t)> const& callback) const
{
    if (symbol_pointer == 0) {
        return false;
    }

    if (!callback(symbol_pointer)) {
        return walk_hash_list(symbol_wrapper{*this, symbol_pointer}.get_next(), callback);
    }

    return false;
}

uint16_t tpu_file::read_word(uint16_t const offset) const
{
    return get_word(_data, offset);
}

uint16_t tpu_file::resolve_code_from_proc(uint16_t const proc_offset) const
{
    // Find segment number
    auto const code_offset = read_word(_proc_map_base + proc_offset + sizeof(uint16_t));
    auto const cseg_offset = read_word(_proc_map_base + proc_offset);

    constexpr auto cseg_entry_size = 8;
    auto const segnum = cseg_offset / cseg_entry_size;

    // Find absolute offset
    constexpr auto cseg_entry_length_offset = 2;
    auto code_pointer = _code_base_para;
    for (int i = 0; i < segnum; ++i) {
        auto const seglen = read_word(_cseg_map_base + i * cseg_entry_size + cseg_entry_length_offset);
        code_pointer += seglen;
    }

    return code_pointer + code_offset;
}