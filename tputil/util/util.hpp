#pragma once

#include <string>
#include <vector>

std::vector<uint8_t> read_binary_file(std::string const& path);

std::vector<uint8_t> to_pascal_string(std::string const& cppstr);
std::string from_pascal_string(std::vector<uint8_t> const& buffer, size_t pos);

uint16_t get_word(std::vector<uint8_t> const& buffer, size_t pos);
