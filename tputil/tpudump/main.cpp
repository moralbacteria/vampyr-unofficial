#include <util/tpu_file.hpp>

#include <algorithm>
#include <iomanip>
#include <iostream>

int main(int argc, char** argv)
{
    if (argc < 2) {
        std::cerr << "Usage: " << argv[0] << " TPUFILE\n";
        return 1;
    }

    auto const filename = argv[1];

    try {
        tpu_file const tpu(read_binary_file(filename));
        auto all_functions = tpu.get_all_functions();
        std::sort(all_functions.begin(), all_functions.end(),
            [](tpu_file::NameOffsetPair const& a, tpu_file::NameOffsetPair const& b) {
                return a.offset < b.offset;
            });

        for (auto const func : all_functions) {
            std::cout << std::hex << func.offset << '\t' << func.name << '\n';
        }
    } catch (std::exception const& e) {
        std::cerr << "EXCEPTION: " << e.what() << std::endl;
        throw;
    }

    return 0;
}
