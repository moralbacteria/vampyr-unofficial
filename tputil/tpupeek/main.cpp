#include <util/tpu_file.hpp>

#include <iomanip>
#include <iostream>

int main(int argc, char** argv)
{
    if (argc < 3) {
        std::cerr << "Usage: " << argv[0] << " TPUFILE SYMBOL\n";
        std::cerr << "SYMBOL must be UPPERCASE\n";
        return 1;
    }

    auto const filename = argv[1];
    auto const symbol = argv[2];
    std::cout << "Finding " << std::quoted(symbol) << " in " << std::quoted(filename) << '\n';

    try {
        tpu_file const tpu(read_binary_file(filename));
        auto const code_pointer = tpu.find_code_pointer(symbol);
        if (code_pointer == 0) {
            std::cerr << "Failed to find\n";
            return 1;
        }

        std::cout << "Found at: 0x" << std::hex << code_pointer << '\n';
    } catch (std::exception const& e) {
        std::cerr << "EXCEPTION: " << e.what() << std::endl;
        throw;
    }

    return 0;
}
