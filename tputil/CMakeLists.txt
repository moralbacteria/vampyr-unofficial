cmake_minimum_required(VERSION 3.10)
project(tpuutil CXX)

add_library(util STATIC util/util.cpp util/tpu_file.cpp)
target_include_directories(util PUBLIC .)

add_executable(tpupeek tpupeek/main.cpp)
target_link_libraries(tpupeek PRIVATE util)

add_executable(tpudump tpudump/main.cpp)
target_link_libraries(tpudump PRIVATE util)